; Copyright (c) September 2008, Roberto García López
; All rights reserved.
; 
; web: https://bitbucket.org/developer2developer/elfcodegenerator
;
; This source file is provided as full example for the tutorial #4 in the article "Creating ELF relocatable object files in Linux".
;
; The article is available at 
; http://developer2developer.wordpress.com/2014/10/01/creating-elf-relocatable-object-files-in-linux/
;
; Note: This file is released under the terms of the GPL2 license.


; Tutorial #4. Calling internal procedures.
;
; This tutorial show you how to call an internal procedure


;;;;;;;
;  Compile with the following command line
;     nasm -f elf skeleton.4.asm


;;;;;;;
;  Link with the following command line
;     ld -s -o skeleton.4 skeleton.4.o



section .data
	msg:	db "Hello World", 10		;  10 = line feed (lf)
	len:	dd $ - msg					; "$" means "here"

section .text

	global _start   ; must be declared for the linker's entry point (ld)

_start:

	; prepare to call the procedure "_printstr"
		push	msg
		push	dword [len]
	; call the procedure
		call	_printstr

	mov	eax, 1	; system call number (sys_exit)
	int	0x80

; procedure _printstr
_printstr:
	push	ebp
	mov	ebp, esp

	mov	eax,4					; write sysout command to int 80 hex
	mov	ebx,1					; arg1, where to write, stdout
	mov	ecx, [ebp + 12]	; The second item (in reverse order) in the stack is the pointer to the string
	mov	edx, [ebp + 8]		; The first item (in reverse order) in the stack is the length of the string
									; [ebp + 4] contains the old ebp
									; [ebp] contains the return of the procedure
	int	0x80					; interrupt 80 hex, call kernel

	leave
	ret
