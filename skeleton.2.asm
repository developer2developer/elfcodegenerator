; Copyright (c) September 2008, Roberto García López
; All rights reserved.
; 
; web: https://bitbucket.org/developer2developer/elfcodegenerator
;
; This source file is provided as full example for the tutorial #2 in the article "Creating ELF relocatable object files in Linux".
;
; The article is available at 
; http://developer2developer.wordpress.com/2014/10/01/creating-elf-relocatable-object-files-in-linux/
;
; Note: This file is released under the terms of the GPL2 license.


; Tutorial #2. Printing a string.
;
; This tutorial show you how to print a string calling the kernel and incorporate the .data section


;;;;;;;
;  Compile with the following command line
;     nasm -f elf skeleton.2.asm


;;;;;;;
;  Link with the following command line
;     ld -s -o skeleton.2 skeleton.2.o



section .data		; data section
	msg:	db "Hello World", 10		; 10 = line feed (lf)

section .text

	global _start   ; must be declared for the linker's entry point (ld)

_start:
	mov	ecx, msg		; pointer to string
	mov	edx, 12		; length of string to print
	mov	ebx, 1		; where to write, stdout
	mov	eax, 4		; write sysout command to int 80 hex	
	int	0x80			; interrupt 80 hex, call kernel

	mov	eax, 1		; system call number (sys_exit)
	int	0x80
