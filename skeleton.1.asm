; Copyright (c) September 2008, Roberto García López
; All rights reserved.
; 
; web: https://bitbucket.org/developer2developer/elfcodegenerator
;
; This source file is provided as full example for the tutorial #1 in the article "Creating ELF relocatable object files in Linux".
;
; The article is available at 
; http://developer2developer.wordpress.com/2014/10/01/creating-elf-relocatable-object-files-in-linux/
;
; Note: This file is released under the terms of the GPL2 license.


; Tutorial #1. Returning the control to the operating system.
;
; This tutorial show you how to return the control to the operating system.


;;;;;;;
;  Compile with the following command line
;     nasm -f elf skeleton.1.asm


;;;;;;;
;  Link with the following command line
;     ld -s -o skeleton.1 skeleton.1.o



section .text

	global _start   ; must be declared for the linker's entry point (ld)

_start:

	mov	eax, 1	; system call number (sys_exit)
	int	0x80
