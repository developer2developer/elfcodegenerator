; Copyright (c) October 2014, Roberto García López
; All rights reserved.
; 
; web: https://bitbucket.org/developer2developer/elfcodegenerator
;
; This source file is provided as full example for the tutorial #5 in the article "Creating ELF relocatable object files in Linux".
;
; The article is available at 
; http://developer2developer.wordpress.com/2014/10/01/creating-elf-relocatable-object-files-in-linux/
;
; Note: This file is released under the terms of the GPL2 license.


; Tutorial #5. Calling external functions.
;
; This tutorial show you how to make a function call to libc.so


;;;;;;;
;  Compile with the following command line
;     nasm -f elf skeleton.5.asm


;;;;;;;
;  Link the generated file with the following command line
;     ld -s -o skeleton.5 /usr/lib/i386-linux-gnu/crti.o /usr/lib/i386-linux-gnu/crt1.o skeleton.5.o /usr/lib/i386-linux-gnu/crtn.o -I/lib/ld-linux.so.2 -lc



; Equivalent C code
;
; #include <stdio.h>
;
; int main()
; {
;   int a = 5;
;   printf("a = %d\n", a);
;   return 0;
; }

; Output:	a = 5



; Declare printf as external (the linker will resolve the reference)
;

	extern	printf	; the C function, to be called

section .data			; Data section, initialized variables

	a:		dd		5						; int a=5;
	fmt:	db		"a = %d", 10, 0	; The printf format, "\n", '0'

section .text			; Code section.

	global main		; Default entry point for the linker (ld)

main:

	push	ebp			; set up stack frame
	mov	ebp, esp

	push	dword [a]	; value of variable a
	push	dword fmt	; address of ctrl string
	call	printf		; Call C function
	add	esp, 12		; pop stack 3 push times 4 bytes (restore the stack pointer)

	mov	esp, ebp		; takedown stack frame
	pop	ebp			; same as "leave" op

	mov	eax, 1		; system call number (sys_exit)
	int	0x80

